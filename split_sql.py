
## start in state NORMAL
## from NORMAL we can enter SIMPLE_COMMENT, BRACKETED_COMMENT, QUOTE_D, QUOTE_S and BACKQUOTE
## from the last 4 states, we can only stay in the same state or revert to NORMAL
## ';' only terminates a statement in state NORMAL


def split_sql(query_string):
    statements = []   ## list of complete sql statements
    partial = []    ## partial statement collected so far
    state = 'NORMAL'    ## initial state
    for char in query_string:
        partial.append(char)
        
        if state == 'NORMAL':  ## not in a comment or string
            ## possible state change characters
            if char == '-':  ## possible start of -- comment
                state = 'dash'
            elif char == '/':  ## possible start of /* comment
                state = 'slash'
            elif char == '"':  ## start of "string"
                state = 'STRING_D'
            elif char == "'":  ## start of 'string'
                state = 'STRING_S'
            elif char == '`':  ## start of `identifier`
                state = 'BACKQUOTE'            
            elif char == ';':  ## in NORMAL mode ';' terminates a statement
		single_statement = ''.join(partial[:-1]).strip()  ## drop final ';' and strip whitespace
		if len(single_statement) > 0:  ## don't append blank statements
                    statements.append(single_statement)
                partial = []    ## initialize for next statement
		char = ''
            else:  ## no state change
                pass

        ## outside NORMAL state we only deal with state changes, no statement terminations
        elif state == 'STRING_d':  ## inside "string", '"' ends the string, '\' escapes next char
            if char == '"':
                state = 'NORMAL'
            elif char == '\\':   ## first \ is Python escape
                state = 'string_esc'
            else:  ## stay in STRING_D
                pass
        elif state == 'STRING_S':  ## inside 'string', same as STRING_D, only difference is state exit by single-quote
            if char == "'":
                state = 'NORMAL'
            elif char == '\\':
                state = 'string_esc'
            else:  ## stay in STRING_S
                pass
        elif state == 'BACKQUOTE':  ## TODO: lookup escape rules for backquotes
            if char == '`':
                state = 'NORMAL'
            else:  ## no state-change
                pass
        elif state == 'dash':
            if char == '-':  
                state = 'SIMPLE_COMMENT'
            else:
                state = 'NORMAL'
        elif state == 'SIMPLE_COMMENT':
            if char == '\n':  ## only exit from -- comment is \n
                state = 'NORMAL'
            else:  ## no state-change
                pass
        elif state == 'slash':
            if char == '*':  ## /* comment
                state = 'BRACKETED_COMMENT'
            else:
                state = 'NORMAL'
        elif state == 'BRACKETED_COMMENT':
            if char == '*':  ## only exit from /* is */
                state = 'asterix'
        elif state == 'asterix':
            if char == '/':  ## */ -> exit comment
                state = 'NORMAL'
            elif char == '*':
                state = 'asterix'
            else:  ## stay in comment
                state = 'BRACKETED_COMMENT'

    ## handle final statement after reading entire input
    ## if last state was NORMAL and last char was ';' we already handled it
    single_statement = ''.join(partial).strip()  ## strip whitespace
    if len(single_statement) > 0:  ## don't append blank statements
        statements.append(single_statement)

    return statements


from __future__ import division
import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy.special import boxcox1p, inv_boxcox1p
import sqlite3
import six
import gc


z1 = pd.read_csv("tmp_volume_call.csv")
z0 = z1.copy()
z1.columns = ['series', 'datetime', 'value']
z1 .dropna(subset=['series', 'datetime'], inplace=True)
z1 .drop_duplicates(subset=['series', 'datetime'], inplace=True)
z1 .sort_values(['series', 'datetime'], inplace=True)


def df_to_tsdict(df):
    """ convert dataframe of time series to dictionary of time series
    """
    cols = df.columns
    df[cols[1]] = pd.to_datetime(df[cols[1]])
    out = { s: df \
                .loc[df[cols[0]]==s, :] \
                .set_index(cols[1]) \
                [cols[2]] \
                .rename((s, cols[0], cols[2])) \
                .sort_index()
            for s in df[cols[0]].unique()
    }
    return out

def tsdict_to_df(tsdict):
    out = pd.concat(tsdict)
    out = out.reset_index()
    y = tsdict.values()[0]
    out.columns = [y.name[1], y.index.name, y.name[2]]
    out.iloc[:,1] = out.iloc[:,1].astype('str')
    return out



def df_ts_asfreq(df, freq='D'):
    tsdict = df_to_tsdict(df)
    out = {s: tsdict[s].asfreq(freq) for s in tsdict}
    out = tsdict_to_df(out)
    return out


def df_ts_extend(df, freq='D', periods=1):
    maxdate = (df.groupby('series')['datetime'].max() + pd.Timedelta(periods, freq)) .reset_index()
    df = df.append(maxdate, ignore_index=True) [['series','datetime','value']]
    out = df_ts_asfreq(df, freq=freq)
    return out


z1['datetime'] = pd.to_datetime(z1['datetime'])

#z2 = z1 .set_index(['series', 'datetime']) ['value']
## max date per series
#md2 = z2 .groupby(level='series') .idxmax()

#maxdate = (z1.groupby("series")['datetime'].max() + pd.Timedelta(30,'D') ).reset_index()
#z2 = z1.append(maxdate, ignore_index=True) [['series','datetime','value']]


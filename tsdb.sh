#!/bin/bash
set -euo pipefail

DB='/tmp/test1.db'
TBL='test'

sqlite3 "$DB" <<- SQL
  create table if not exists "$TBL" (
    series text not null,
    datetime text not null,
    value numeric,
    primary key (series,datetime) on conflict replace
  );
SQL


sqlite3 "$DB" <<- SQL
  create table if not exists "${TBL}_forecast" (
    series text not null,
    datetime text not null,
    predicted numeric default value null,
    horizon integer not null,
    method text not null,
    primary key (series,datetime,method) on conflict replace
  );
SQL


sqlite3 "$DB" <<- SQL
  create table if not exists "${TBL}_forecast_test" (
    series text not null,
    datetime text not null,
    predicted numeric default value null,
    horizon integer not null,
    method text not null,
    primary key (series,datetime,horizon,method) on conflict replace
  );
SQL



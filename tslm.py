import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy.special import boxcox1p, inv_boxcox1p

def logerr():
    pass


def tbl_stats_pd(df, dropna=True):
    grouped = df.groupby(df.columns[0])
    ## row_count and null_count
    sizes = grouped.size()
    counts = grouped.count() 
    counts.columns += '_nullcount'
    counts = ( sizes.values - counts.T ).T
    counts.insert(0,'rowcount', sizes)
    ## alt
    counts = grouped.agg(['size','count'])
    nullcounts = counts.xs('size',1,1) - counts.xs('count',1,1) 
    nullcounts.columns += 'nullcount'
    nullcounts.insert(0,'rowcount', counts.iloc[:,0])

    ## first and last valid
    valid = df.dropna(subset=[df.columns[2]])  ## remove NULL on 3rd column
    first_last = valid.drop_duplicates([df.columns[0]],'first') .merge( 
            valid.drop_duplicates([df.columns[0]],'last'), on=df.columns[0], validate='1:1', suffixes=('_first','_last'))
    
    ## volume-min/max (for log-transform)? or handle this per-series

    ## non-unique series/datetime
    return dfc

    
def get_outlier(y, freq='D'):
    y.index = pd.to_datetime(y.index)
    lny = np.log(y+0.1)
    lnym = lny.rolling(14, center=True).median()
    dow = (lny - lnym).groupby(lny.index.weekday_name).median()
    lnym += pd.Series(lny.index.weekday_name, index=lny.index) .replace(dow)
    err = lny - lnym
    outl_n = err[err < err.quantile(0.025)*2]
    outl_p = err[err > err.quantile(0.975)*2]
    outl = outl_n .append(outl_p)
    outl = pd.Series('' + outl.index.astype('str').str[-5:], index=outl.index)
    return outl


## in: df['series','datetime','value', 'events', ...]   ## first 3 cols are interpreted
## out: (pts, err, optionals)
def df_to_timeseries(df, freq='D'):
    y = pd.Series()
    ## ensure we have a DF of at least shape (1,3)
    if not isinstance(df, pd.DataFrame) or df.shape[0] < 1 or df.shape[1] < 3:
        logerr()
    ##  check if first column contains single string or integer
    col = df.columns
    series = df[col[0]].unique()
    if series.size !=1 or not (isinstance(series, str) or isinstance(series, int)):
        logerr()
    y = df .set_index(col[1], verify_integrity=True) [col[2]] .sort_index()
    y.name = (series[0], col[0], col[2])  ## store lables in name attribute to round_trip back to DF
    y.index = pd.to_datetime(y.index, infer_datetime_format=True)
    ## TODO: detect freq
    y = y.asfreq(freq)
    y.index.name = (col[1], freq)
    ## TODO: errors and optionals
    return y



def detect_freq(ix):
    ## return: 'Y', 'Q', 'M', 'W', 'D', 'DY', 'DQ', 'DM', 'DW', 'hr', 'min', 'sec'
    return 'D'


def x_trend_polynomial(ix, order=1):
    x = np.arange(ix.size)
    X = pd.DataFrame(index=ix)
    for i in range(0, order+1):
        X['trend_{}'.format(i)] = x**i
    return X

def x_trend_linspline(ix, knots, incremental=False):
    ## todo: sort knots
    X = x_trend_polynomial(ix, order=1)
    x = X.iloc[:,-1]
    for k in knots:
        x0 = x[k].iat[0] if isinstance(x[k], pd.Series) else x[k]
#        X.loc[k:,X.columns[-1]] = x[k]
        X['trend_{}'.format(str(k))] = x[k:] - x0 + 1
    return X.fillna(0)
#    for k in knots:
#        x0 = x[k].iat[0] if isinstance(x[k], pd.Series) else x[k]
#        xn = X.iloc[:,-1][k:]
#        X.loc[xn.index, xn.name] = xn[k]
#        X['trend_{}'.format(str(k))] = xn
##        X.loc[ xn.notna()  , X.columns[-1]] = np.nan
    return X.fillna(0)


def x_dummies(ix, s):
    X = pd.DataFrame(pd.get_dummies(s), index=ix) .fillna(0)
    return X


def x_cycle_weekly(ix):
    X = pd.get_dummies(ix.weekday_name.str[:3], prefix='D')
    X.index = ix
    X['D_Sun'] = -1
    X.loc[X.index.weekday_name=='Sunday','D_Sun'] = 0
    return X[['D_Mon','D_Tue','D_Wed','D_Thu','D_Fri','D_Sat','D_Sun']]

def x_cycle_yearly(ix):
    X = pd.get_dummies(ix.month, drop_first=True, prefix='month', index=ix)
    X.index = ix
    return X

def x_cycle_trig(ix, period=365.25, terms=4):
    if isinstance(ix.name, tuple):
        ix.name = ix.name[0]
    freq = 2*np.pi / period
    j = pd.Series( (ix - ix[0]) / pd.Timedelta('1 day') , index=ix)
    x = j.to_frame()
    for k in range(1,terms+1):
        x['cos_'+str(k)] = np.cos(j*k*freq)
        x['sin_'+str(k)] = np.sin(j*k*freq)
    x = x.iloc[:,1:]
    return x


def prophet_knots(y):
    ix_last = y[y.first_valid_index():y.last_valid_index()].size * 0.8
    step = ix_last / 25.0
    ix = np.arange(step, step*26, step).round().astype('i')
    return y.index[ix].astype('str')


def m_spline(y, changepoints):
    y = y[y.first_valid_index():]
    outlier = get_outlier(y)
    
    x_outlier = x_dummies(y.index, outlier)
    x_week = x_cycle_weekly(y.index)
    x_year = x_cycle_trig(y.index, terms=8)
    
    x_trend = x_trend_linspline(y.index, knots=changepoints)
    X = pd.concat([x_trend, x_year, x_week, x_outlier], axis=1)

    m = sm.OLS(y, X, missing='drop').fit()
    return m


def test_m(y):
    cp1 = prophet_knots(y)
    m1 = m_spline(np.log(y+1), cp1)
    ## prume changepoints
    p1 = m1.params[m1.tvalues.abs()<2.0].index
    p1 = p1[p1.str.startswith('trend_')] .str[6:16] .astype('str')
    cp2 = cp1.astype('str').to_series()
    cp2 = cp2 [~cp1.isin(p1)] .tolist()
    m2 = m_spline(np.log(y+1), cp2)
    return (m1,m2)



def x_all(y):
    y = y[y.first_valid_index():]
    y0 = y.copy()
    outlier = get_outlier(y)

    y = np.log(y0+1.0)
    x_outlier = x_dummies(y.index, outlier)
    x_week = x_cycle_weekly(y.index)
    x_year = x_cycle_trig(y.index, terms=8)
    x_trend = x_trend_linspline(y.index, knots=[])
    X = pd.concat([x_trend, x_year, x_week, x_outlier], axis=1)
    m1 = sm.OLS(y, X, missing='drop').fit()

    y = y0
    x_outlier = x_dummies(y.index, outlier)
    x_week = x_cycle_weekly(y.index)
    x_year = x_cycle_trig(y.index, terms=8)
    x_trend = x_trend_linspline(y.index, knots=[])
    X = pd.concat([x_trend, x_year, x_week, x_outlier], axis=1)
    m2 = sm.OLS(y, X, missing='drop').fit()

    y = boxcox1p(y0, 0.2)
    x_outlier = x_dummies(y.index, outlier)
    x_week = x_cycle_weekly(y.index)
    x_year = x_cycle_trig(y.index, terms=8)
    x_trend = x_trend_linspline(y.index, knots=[])
    X = pd.concat([x_trend, x_year, x_week, x_outlier], axis=1)
    m3 = sm.OLS(y, X, missing='drop').fit()
    
    return (m1, m2, m3)


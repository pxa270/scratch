
import pandas as pd
import namedtuple


df_log = pd.DataFrame(columns=['pyfunc','tbl_col_row','level','msg'])


def clean1(df0, df_log):
    if not isinstance(pdf, pd.DataFrame):
        ## log
        return (None, df_log)
    if not pdf.shape[0] >= 3:
        return 'err_cols'
    if not pdf.shape[1] > 0:
        return 'err_empty'
    ## handle nulls in col0 or col1 (remove and log warning)
    ## check col2 dtype numeric
    ## recheck rows after removal
    

def clean2(pdf):
    ## we know df is non-empty and col dtypes are ok, so now we split on col0
    return None

def detect_freq(ix):
    ## return: 'Y', 'Q', 'M', 'W', 'D', 'DY', 'DQ', 'DM', 'DW', 'hr', 'min', 'sec'
    return 'D'



def pdf2pdts(pdf2, as_pdf=False, asfreq=None):
    ## assume cleaned and splitted pdf2 with only 1 value in col0
    pts = pd.Series( data = pdf2.iloc[:,2].values,
                     index = pdf2.iloc[:1]
                     name = (pdf.columns[0], pdf.iat[0,0])  ## or other way around??
                     ) .sort_index()

    pts = pts[pts.first_valid_index():pts.last_valid_index()]
    freqstr = detect_freq(pts.index)
    pts = pts.astype(freqstr)
    pts.index.name = (pts.index.name, freqstr)
    return pts

def tsstats(pts, extended=False):
    ## per series: (id, nrow, nulls, first_dt, first_val, last_dt, last_val, min_val,min_dt, max_val, max_dt)
    ##
    ss = namedtuple('series_summary', ['series_id', 'row_count', 'null_count', 'freq',
                                       'first_datetime', 'first_value', 'last_datetime', 'last_value', 
                                       'min_value', 'min_value_datetime', 'max_value', 'max_value_datetime' ] )
    ix_str = pts.index.astype('str')
    v_min = pts.values.min()
    v_max = pts.values.max()
    return ss( pts.name, pts.size, pts.isnull().sum(), pts.index.freqstr
        ix_str[0], pts.iat[0], ix_str[-1], pts.iat[-1],
        v_min, pts[pts==v_min].index.astype('str')[-1],
        v_max, pts[pts==v_max].index.astype('str')[-1] )
           

def full_period_agg(pdf3):
    return

## in: hivedf
## out: clean_pdf+warnings/errors  or tsdict

## fc df_in:  [series,datetime,value] [s,t,v], optional [x1, x2, x3]
## fc_df_out: [s,t,v, t_k]

def isocal2dt(ix):
    return

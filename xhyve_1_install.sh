#!/bin/bash

set -euo pipefail

IN='debian-9.9.0-amd64-netinst.iso'
OUT='debian9.img'

TARGETDIR=$(basename -s .iso $IN)
OUT="$TARGETDIR/$OUT"

extract_boot() {
  dd if=/dev/zero of=tmp.iso bs=$[2*1024] count=1
  dd if="$IN" bs=$[2*1024] skip=1 >> tmp.iso

  diskinfo=$(hdiutil attach tmp.iso)

  set +e
  mkdir -p "$TARGETDIR"
  #mnt=$(echo "$diskinfo" | perl -ne '/(\/Volumes.*)/ and print $IN')
  mnt=$(grep -oe "Volumes.*" <<< "$diskinfo")

  cp "/$mnt/install.amd/vmlinuz" "$TARGETDIR"
  cp "/$mnt/install.amd/initrd.gz" "$TARGETDIR"
  set -e

  disk=$(echo "$diskinfo" |  cut -d' ' -f1)
  hdiutil eject "$disk"
  rm tmp.iso
}

check_root() {
  if [ "$(whoami)" != "root" ]; then
      echo "missing sudo"
      exit 1
  fi
}

install_it() {
#  check_root

  storagegb=4
  memgb=1
  cpus=1
  MB=$[1024*1024]
  GB=$[1024*$MB]

  dd if=/dev/zero of="$OUT" bs=$[1*$GB] count=$storagegb

  sudo xhyve \
    -A \
    -c "$cpus" \
    -m "${memgb}G" \
    -s 0,hostbridge \
    -s 2,virtio-net \
    -s "3,ahci-cd,$IN" \
    -s 4,virtio-blk,"$OUT" \
    -s 31,lpc \
    -l com1,stdio \
    -f "kexec,$TARGETDIR/vmlinuz,$TARGETDIR/initrd.gz,earlyprintk=serial console=ttyS0"
}


#install_it


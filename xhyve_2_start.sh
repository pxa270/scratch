#!/bin/bash

set -euo pipefail

IN='debian-9.9.0-amd64-netinst.iso'
OUT='debian9.img'

TARGETDIR=$(basename -s .iso $IN)
OUT="$TARGETDIR/$OUT"

#  storagegb=4
memgb=1
cpus=1



sudo xhyve \
    -A \
    -U $(cat $OUT.uuid) \
    -c "$cpus" \
    -m "${memgb}G" \
    -s 0,hostbridge \
    -s 2,virtio-net \
    -s 4,virtio-blk,"$OUT" \
    -s 31,lpc \
    -l com1,stdio \
    -f "kexec,$TARGETDIR/vmlinuz,$TARGETDIR/initrd.gz,root=/dev/vda1 ro"
#    -f "kexec,$TARGETDIR/vmlinuz,$TARGETDIR/initrd.gz,earlyprintk=serial console=ttyS0 root=/dev/vda1 ro"

##  headless: 	omit console=ttyS0
